module.exports = function(eleventyConfig) {
    eleventyConfig.addPassthroughCopy("src/images");

    eleventyConfig.addCollection("news", collection => {
        return collection.getFilteredByGlob("src/news/*.md");
    });

    eleventyConfig.addCollection("events", collection => {
        return collection.getFilteredByGlob("src/events/*.md");
    });

    eleventyConfig.addCollection("sponsors", collection => {
        return collection.getFilteredByGlob("src/sponsors/*.md");
    });

    eleventyConfig.addCollection("boards", collection => {
        return collection.getFilteredByGlob("src/honourboards/*.md");
    });

    eleventyConfig.addCollection("contacts", collection => {
        return collection.getFilteredByGlob("src/contacts/*.md");
    });

    return {
        dir: {
            input: "src",
            output: "dist",
            data: "_data",
            includes: "templates"
        },
        passthroughFileCopy: true,
        templateFormats: ["njk", "md", "css", "html", "yml"],
        htmlTemplateEngine: "njk"
    };
};
