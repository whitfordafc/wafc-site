---
layout: layouts/base.html
title: Whitford AFC
about: "### All for one and one for All\r\n\r\nWe are an Amatuer *Football* and *Netball* Club based in Padbury, Western Australia. Our Football Club was founded in 1976 and has enjoyed success across multiple grades. The Netball Club was founded in 2013 and has also enjoyed success across the grades within the short timespan."
---

{{ about }}
