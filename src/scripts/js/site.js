//init functions
$(document).ready(function() {
    setupMenu();
    // initMap();
    scrollToTop();
    dataAos();
});

//functions start here

function initMap() {
    // The location of wafc
    var wafc = { lat: -31.80399, lng: 115.757 };
    // The map, centered at wafc
    var map = new google.maps.Map(document.getElementById("map"), {
        zoom: 15,
        center: wafc
    });

    // The marker, positioned at wafc
    var marker = new google.maps.Marker({
        draggable: false,
        animation: google.maps.Animation.DROP,
        position: wafc,
        map: map,
        icon:
            "https://www.whitfordafc.com.au/wp-content/themes/badfunkstripe/img/icons/logo.svg",
        mapTypeControl: false
    });
}

function setupMenu() {
    $(".menu-icon").on("click", function() {
        var $this = $(this);
        var $that = $(".menu");

        $this.toggleClass("open");
        $that.toggleClass("open");
    });

    $(".menu__close-btn").on("click", function() {
        $(".menu").toggleClass("open");
    });
}

function scrollToTop() {
    $(".scroll-top").on("click", function(e) {
        e.preventDefault();
        window.scroll({
            top: 0,
            left: 0,
            behavior: "smooth"
        });
    });
}
